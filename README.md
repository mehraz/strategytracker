# StrategyTracker

How to run:
- change the OUTPUT_DIR and INPUT_DIR in src/main/scala/strategytrack/package.scala
- use command "sbt run"

Note: after running the server, edit the data.csv file and then copy it into the INPUT_DIR folder. Otherwise, Spark will not pick it up for process.

