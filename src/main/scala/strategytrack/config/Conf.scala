package strategytrack.config

import org.apache.spark.SparkConf

object Conf {
  lazy val localSparkConf = new SparkConf().setMaster("local[2]").setAppName("Strategy Tracker")
}


