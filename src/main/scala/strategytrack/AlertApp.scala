package strategytrack

import org.apache.spark.streaming.dstream.DStream
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.{SparkConf}
import strategytrack.config.Conf

/**
  * Use this to test the app locally, from sbt:
  * sbt "run pathToFile"
  */
object AlertLocalApp extends App{
  Runner.run(Conf.localSparkConf, Settings.INPUT_DIR)
}


object Runner {

  /**
    *
    * @param conf
    * @param inputDirectory
    * run starts reading the new file (which are edited after the App started) and tracks Strategies
    */
  def run(conf: SparkConf, inputDirectory:String): Unit = {

    val conf = Conf.localSparkConf
    val ssc = new StreamingContext(conf, Seconds(Settings.SPARK_INTERVAL))
    //set Input Stream
    val data = ssc.textFileStream(inputDirectory)
    val lines = data.flatMap(_.split("\n"))
    val spreads: DStream[List[Double]] = Tracker.withSpreadCalculated(lines)
    trackStrategyAt(spreads, strategyNum = 0)
    trackStrategyAt(spreads, strategyNum = 1)
    trackStrategyAt(spreads, strategyNum = 2)

    //start computing the input stream
    ssc.start()
    ssc.awaitTermination()
  }


  /**
    *
    * @param spreads : a table-like spread values for each strategy. Column[0]-> strategy1, Column[1] -> strategy2,
    * @param strategyNum: zero-based strategy index in the input file
    * It assigns a Tracker to each strategy, and computes alert1
    */
  def trackStrategyAt(spreads:DStream[List[Double]], strategyNum:Int) = {
    val windowedStrategy: DStream[Double] = spreads.window(Seconds(Settings.WINDOW_LENGTH),Seconds(Settings.WINDOW_INTERVAL)).map(x=>x(strategyNum))
    Logger.logDStream(windowedStrategy, s"Windowed-Strategy-$strategyNum")

    val alertVal = Tracker.isAlert1(windowedStrategy, 2.5, s"Windowed-Strategy-$strategyNum")
    Logger.logDStream(alertVal, s"Alert1--Windowed-Strategy-$strategyNum")
  }
}
