package strategytrack

import org.apache.spark.rdd._
import org.apache.spark.streaming.dstream.DStream

object Tracker {

  /**
    * For the sake of simplicity, the date and price will be removed and only spread values of strategies return
    * @param stream :DStream[String]
    * @return
    */
  def withSpreadCalculated(stream:DStream[String]): DStream[List[Double]] =
    stream
    .map ( x => {
      val items = x.split(",") :+ "0" //to fix the last item missing
      val price = convertDouble(items.lift(1).getOrElse("0"))
      val stg1 = convertDouble(items.lift(2).getOrElse("0"))
      val stg2 = convertDouble(items.lift(3).getOrElse("0"))
      val stg3 = convertDouble(items.lift(4).getOrElse("0"))
      List(calcSpread(stg1 , price), calcSpread(stg2 , price), calcSpread(stg3 , price))
  })


  /**
    * It returns true if the alert value is larger than the given threshold
    * @param stream :DStream[String]
    * @param threshold :Double
    * @return
    */
  def isAlert1(stream:DStream[Double], threshold:Double, name:String): DStream[Boolean] = {
    calculatingAlert1(stream, name).map(value=> value>threshold)
  }


  private def calculatingAlert1(stream:DStream[Double], name:String) = {
    //calc mean
    val mean = stream
      .map(x=>(x,1))
      .reduce((x,y)=>(x._1+y._1 , x._2+y._2))
      .map(x => x._1 / x._2)
    Logger.logDStream(mean, s"Mean-Value--$name")

    //calc std deviation
    val std =stream.transformWith(mean, (item1:RDD[Double], item2:RDD[Double]) => item1.cartesian(item2) ) // put item and mean together
      .map(x=> Math.pow(x._1-x._2, 2))        //(item - mean)^2
      .reduce(_+_)
      .transformWith(stream.count(), (sum:RDD[Double], count:RDD[Long]) =>  sum.cartesian(count)) //put the sum and stream.count together
        .map(x=> x._1/ x._2)          // calculates sum/count
        .map(Math.sqrt)            //sqrt of the sum
    Logger.logDStream(std, s"STD-Deviation--$name")

    //calc Alert value
    val alertValue = stream.transformWith(std, (item1:RDD[Double], item2:RDD[Double]) => item1.cartesian(item2))
      .map(x=> Math.abs(x._1-x._2))
      .reduce(_+_)
      .transformWith(stream.count(), (sum:RDD[Double], count:RDD[Long]) =>  sum.cartesian(count))
      .map(x=> x._1/ x._2)
    Logger.logDStream(alertValue, s"Alert1-Value--$name")
    alertValue
  }

  /**
    * In case of exception, it will return 0.
    * @param value :String
    * @return
    */
  private def convertDouble(value:String) = try {
    value.toDouble
  } catch {
    case _:Throwable=> 0d
  }

  private def calcSpread(strgy:Double, price:Double): Double = (strgy, price) match {
    case (x, y) if x==0 || y == 0 => 0
    case _ => strgy-price
  }


}
