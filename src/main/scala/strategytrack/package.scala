import org.apache.spark.streaming.dstream.DStream

package object strategytrack {

  object Settings {
    val SPARK_INTERVAL = 10
    val WINDOW_LENGTH = 50
    val WINDOW_INTERVAL = 50
    val OUTPUT_DIR = "file:///home/vagrant/Public/repo/StrategyTracker/result/"
    val INPUT_DIR = "file:///home/vagrant/Public/repo/StrategyTracker/data"
  }


  object Logger {

    def logRDD[T](ds: DStream[T], name: String, count:Int = 1): Unit = {
      ds.foreachRDD(rdd => {
        if (!rdd.isEmpty()) {
          count match {
            case size:Int if size<=1 => rdd.saveAsTextFile(s"${Settings.OUTPUT_DIR}/$name"); println(s"Logging RDD $name is: ${rdd.first()}")
            case size: Int => rdd.saveAsTextFile(Settings.OUTPUT_DIR); println(s"Logging RDD $name is: ${rdd.take(size).toList}")
          }
        }
      })
    }

    def logDStream[T](ds: DStream[T], name: String, saveInFile:Boolean=true) = {
      ds.print()
      if(saveInFile) ds.saveAsTextFiles(s"${Settings.OUTPUT_DIR}<$name>", s"</$name>")
    }
  }

}
